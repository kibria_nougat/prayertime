package com.powergroupbd.prayertime.model;

/**
 * Created by Office on 2/14/2017.
 */

public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}