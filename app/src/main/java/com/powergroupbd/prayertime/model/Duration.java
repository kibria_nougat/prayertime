package com.powergroupbd.prayertime.model;

/**
 * Created by Office on 2/14/2017.
 */

public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
