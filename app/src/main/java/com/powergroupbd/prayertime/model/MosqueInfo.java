package com.powergroupbd.prayertime.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Office on 2/14/2017.
 */

public class MosqueInfo implements Serializable{

    private String mosqueName;
    private String mosqueDistance;
    private String timeToReachMosque;
    private String originAddress;
    private String destinationAddress;
    private String lat;
    private String lng;

    public String getMosqueName() {
        return mosqueName;
    }

    public String getMosqueDistance() {
        return mosqueDistance;
    }

    public String getTimeToReachMosque() {
        return timeToReachMosque;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }


    public MosqueInfo(String mosqueName, String mosqueDistance, String timeToReachMosque, String originAddress, String destinationAddress, String lat, String lng) {
        this.mosqueName = mosqueName;
        this.mosqueDistance = mosqueDistance;
        this.timeToReachMosque = timeToReachMosque;
        this.originAddress = originAddress;
        this.destinationAddress = destinationAddress;
        this.lat = lat;
        this.lng = lng;
    }


}
