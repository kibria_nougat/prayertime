package com.powergroupbd.prayertime.model;

import java.io.Serializable;

/**
 * Created by Office on 2/14/2017.
 */

public class Mosques implements Serializable {

    public String getName() {
        return name;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    String name;
    String lat;
    String lng;

    public Mosques(String name, String lat, String lng){
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

}
