package com.powergroupbd.prayertime.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.powergroupbd.prayertime.R;
import com.powergroupbd.prayertime.model.MosqueInfo;

import java.util.ArrayList;

/**
 * Created by Office on 2/14/2017.
 */

public class MosquesListAdapter extends RecyclerView.Adapter<MosquesListAdapter.MosquesListRecyclerViewHolder> {

    ArrayList<MosqueInfo> mosqueInfoList;
    static SingleEntryClickListener listener;


    public MosquesListAdapter(ArrayList<MosqueInfo> mosqueInfoList, SingleEntryClickListener listener) {
        this.listener = listener;
        this.mosqueInfoList = mosqueInfoList;
    }

    @Override
    public MosquesListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.nearby_mosques_single_layout, parent, false);
        MosquesListRecyclerViewHolder holder = new MosquesListRecyclerViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(MosquesListRecyclerViewHolder holder, int position) {
        holder.mosqueName_TextView.setText(mosqueInfoList.get(position).getMosqueName());
        holder.distance_TextView.setText(mosqueInfoList.get(position).getMosqueDistance() + " km");
        holder.time_TextView.setText(mosqueInfoList.get(position).getTimeToReachMosque());
    }

    @Override
    public int getItemCount() {
        return mosqueInfoList.size();
    }

    public class MosquesListRecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView mosqueName_TextView, distance_TextView, time_TextView;
        LinearLayout singleRow;

        public MosquesListRecyclerViewHolder(View itemView) {
            super(itemView);

            mosqueName_TextView = (TextView) itemView.findViewById(R.id.mosqueName_TextView);
            distance_TextView = (TextView) itemView.findViewById(R.id.distance_TextView);
            time_TextView = (TextView) itemView.findViewById(R.id.time_TextView);
            singleRow = (LinearLayout) itemView.findViewById(R.id.singleRow);

            singleRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSingleEntryClick(v, getAdapterPosition());
                }
            });

        }
    }

    public interface SingleEntryClickListener {
        void onSingleEntryClick(View v, int position);
    }
}
