package com.powergroupbd.prayertime.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.powergroupbd.prayertime.AdhanNotificationActivity;
import com.powergroupbd.prayertime.R;

public class AdhanPlayerService extends Service {
    public AdhanPlayerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    MediaPlayer mp;

//    private void pushServicetoForeground() {
//        final int notiId = 1234;
//        Notification notice;
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(
//                this).setSmallIcon(R.drawable.notification)
//                .setContentTitle(getResources().getString(R.string.app_name))
//                .setContentText(getString(R.string.listening)).setAutoCancel(true);
//
//        Intent notificationIntent = new Intent(this, AdhanNotificationActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 1010,
//                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        builder.setContentIntent(contentIntent);
//        notice = builder.build();
//        startForeground(notiId, notice);
//
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int should_fajr_adhan_play = intent.getIntExtra("should_fajr_adhan_play", 4);
        int should_play_adhan_or_default_alarm = intent.getIntExtra("should_play_adhan_or_default_alarm", 6);
        int is_play_nothing = intent.getIntExtra("Is_play_nothing", 0);


        if (should_fajr_adhan_play == 1) {

            if (should_play_adhan_or_default_alarm == 1) {

                mp = MediaPlayer.create(this, Uri.parse("android.resource://com.powergroupbd.prayertime/raw/fajr_adhan"));

                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopSelf();
                    }

                });

                mp.start();

            } else {

                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                mp = MediaPlayer.create(this, notification);

                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopSelf();
                    }

                });

                mp.start();

            }
        } else {

            if (should_play_adhan_or_default_alarm == 1) {

                mp = MediaPlayer.create(this, Uri.parse("android.resource://com.powergroupbd.prayertime/raw/islam_azan"));

                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopSelf();
                    }

                });

                mp.start();

            } else {

                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                mp = MediaPlayer.create(this, notification);
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopSelf();
                    }

                });

                mp.start();

            }

        }

        if (is_play_nothing == 1) {

        }

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        if (mp != null) {

            if (mp.isPlaying()) {

                mp.stop();

            }
            mp.release();
            mp = null;

        }
        super.onDestroy();
    }
}
