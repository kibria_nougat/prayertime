package com.powergroupbd.prayertime.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.powergroupbd.prayertime.R;

/**
 * Created by Office on 2/6/2017.
 */

public class FragmentOthers extends Fragment implements View.OnClickListener{

    LinearLayout shareLayout, rateLayout, otherAppsLayout;

    public static FragmentOthers newInstance() {
        return new FragmentOthers();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_others, container, false);


        shareLayout = (LinearLayout) view.findViewById(R.id.shareLayout);
        rateLayout = (LinearLayout) view.findViewById(R.id.rateLayout);
        otherAppsLayout = (LinearLayout) view.findViewById(R.id.otherAppsLayout);

        shareLayout.setOnClickListener(this);
        rateLayout.setOnClickListener(this);
        otherAppsLayout.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.shareLayout:

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String myText = getResources().getString(R.string.app_name) + "\n" + "https://play.google.com/store/apps/details?id=com.powergroupbd.prayertime";
                sharingIntent.putExtra(Intent.EXTRA_TEXT, myText);
                startActivity(Intent.createChooser(sharingIntent, ""));
                break;

            case R.id.rateLayout:

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.powergroupbd.prayertime")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.powergroupbd.prayertime")));
                }
                break;

            case R.id.otherAppsLayout:

                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://search?q=powerandroidbd"));
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store?hl=en")));
                }
                break;

        }
    }
}
