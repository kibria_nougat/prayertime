package com.powergroupbd.prayertime.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.powergroupbd.prayertime.AppController;
import com.powergroupbd.prayertime.Constants;
import com.powergroupbd.prayertime.R;

import com.powergroupbd.prayertime.adapter.MosquesListAdapter;
import com.powergroupbd.prayertime.model.MosqueInfo;
import com.powergroupbd.prayertime.model.Mosques;
import com.powergroupbd.prayertime.separator.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Created by Office on 2/6/2017.
 */

public class FragmentMosques extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    GoogleApiClient mGoogleApiClient = null;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1972;
    int PROXIMITY_RADIUS = 5000;
    Location mLastLocation;
    RecyclerView mosques_RecycleListView;
    ArrayList<Mosques> mosquesArrayList = null;
    static ArrayList<MosqueInfo> mosqueInfos = null;
    MosquesListAdapter mosquesListAdapter;
    static int counter = 0;
    LinearLayout goToMap;

    InterstitialAd fullScreenAdd;
    AdRequest fullScreenAdRequest;
    boolean shouldShowAdd = false;
    boolean isActivityRunning = true;

    public static FragmentMosques newInstance() {
        return new FragmentMosques();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mosquesArrayList = new ArrayList<Mosques>();

        getMyLocation();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mosques, container, false);

        mosques_RecycleListView = (RecyclerView) view.findViewById(R.id.mosques_RecycleListView);
        goToMap = (LinearLayout) view.findViewById(R.id.goToMap);

        mGoogleApiClient.connect();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mosques_RecycleListView.setLayoutManager(linearLayoutManager);
        mosques_RecycleListView.setHasFixedSize(true);

        goToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentMap myMapFragment = new FragmentMap();

                Bundle args = new Bundle();
                args.putSerializable("MosquesInfoList", mosqueInfos);
                args.putInt("Position", -1);
                myMapFragment.setArguments(args);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_activity_frameLayout, myMapFragment);
                transaction.commit();
            }
        });

        return view;
    }


    private void getMyLocation() {
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.d("MyOnConnected", "On Connected is Called");

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {

            Log.d("MyOnConnected", "Permission Granted");

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                Log.d("MyOnConnected", "Last Location found");

                String lat = String.valueOf(mLastLocation.getLatitude());
                String lng = String.valueOf(mLastLocation.getLongitude());
//                  Toast.makeText(getActivity(), "lat: " + lat + "\n lng: " + lng, Toast.LENGTH_SHORT).show();
                makePlaceRequest(lat, lng);

            }
        }
    }

    public void makePlaceRequest(String lat, String lng) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + lat + "," + lng);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&type=" + "mosque");
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + Constants.GOOGLE_API_KEY);

        Log.d("googlePlacesUrl", googlePlacesUrl + "");

        GetMosquesList getMosquesList = new GetMosquesList(getActivity());
        String origin = lat + "," + lng;
        getMosquesList.getMosquesListFromGooglePlaceApiRequest(origin, googlePlacesUrl.toString());
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public class GetMosquesList {

        Context mContext;
        MaterialDialog materialDialog;


        public GetMosquesList(Context context) {
            mContext = context;
        }

        public void getMosquesListFromGooglePlaceApiRequest(final String origin, String url) {

            if (mosquesArrayList != null) {
                mosquesArrayList.clear();
            }
            Log.d("URL", url);
            showpDialog();
            JsonObjectRequest getSingleDayPrayerTimesRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responseObject) {

                    try {
                        Log.d("ResponseStatus", "Responded");
                        JSONArray results = responseObject.getJSONArray("results");

                        for (int i = 0; i < results.length(); i++) {

                            JSONObject jsonObject = results.getJSONObject(i);

                            JSONObject geometry = jsonObject.getJSONObject("geometry");
                            JSONObject location = geometry.getJSONObject("location");

                            String lat = location.getString("lat");
                            String lng = location.getString("lng");

                            String name = jsonObject.getString("name");

                            Log.d("ResponseStatus", name);
                            Mosques singleMosque = new Mosques(name, lat, lng);
                            mosquesArrayList.add(singleMosque);

                        }

                        getDistanceAndTimeForEachMosque(origin, mosquesArrayList);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("NetWorkError", error.toString());
                    Toast.makeText(getActivity(), "Network error!", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(getSingleDayPrayerTimesRequest);
        }

        private void showpDialog() {
            materialDialog = new MaterialDialog.Builder(getActivity())
                    .title(R.string.nearbymosque)
                    .content(R.string.pleasewait)
                    .progress(true, 0)
                    .backgroundColor(Color.parseColor("#ffffff"))
                    .contentColor(Color.parseColor("#37474f"))
                    .titleColor(Color.parseColor("#263238"))
                    .cancelable(true)
                    .widgetColor(Color.parseColor("#1d8049"))
                    .show();
        }

        private void hidepDialog() {
            materialDialog.dismiss();
        }

        private void getDistanceAndTimeForEachMosque(String origin, final ArrayList<Mosques> mosquesArrayList) {

            mosqueInfos = new ArrayList<MosqueInfo>();
            if (mosqueInfos != null) {
                mosqueInfos.clear();
            }
            counter = 0;

            for (int i = 0; i < mosquesArrayList.size(); i++) {


                float[] results = new float[1];

                String[] latlng = origin.split(",");

                Location.distanceBetween(
                        Double.parseDouble(latlng[0]),
                        Double.parseDouble(latlng[1]),
                        Double.parseDouble(mosquesArrayList.get(i).getLat()),
                        Double.parseDouble(mosquesArrayList.get(i).getLng()),
                        results);
                double distanceInKM = results[0] / 1000;
                String distance = String.valueOf(round(distanceInKM, 2));
                String duration = "";
                if (distanceInKM < 0.5) {

                    duration = "2min";
                } else if (distanceInKM > 0.5 && distanceInKM < 1.0) {

                    duration = "4min";

                } else if (distanceInKM > 1.0 && distanceInKM < 1.5) {

                    duration = "6min";

                } else if (distanceInKM > 1.5 && distanceInKM < 2.0) {

                    duration = "8min";

                } else if (distanceInKM > 2.0 && distanceInKM < 2.5) {

                    duration = "10min";

                } else if (distanceInKM > 2.5 && distanceInKM < 3.0) {

                    duration = "12min";

                } else if (distanceInKM > 3.0 && distanceInKM < 3.5) {

                    duration = "14min";

                } else if (distanceInKM > 3.5 && distanceInKM < 4.0) {

                    duration = "16min";

                } else if (distanceInKM > 4.0 && distanceInKM < 4.5) {

                    duration = "18min";

                } else if (distanceInKM > 4.5 && distanceInKM < 5.0) {

                    duration = "20min";

                }
                MosqueInfo mosqueInfo = new MosqueInfo(mosquesArrayList.get(counter).getName(), distance, duration, "", "", mosquesArrayList.get(counter).getLat(), mosquesArrayList.get(counter).getLng());
                mosqueInfos.add(mosqueInfo);
                counter++;
                if (counter == mosquesArrayList.size() - 1) {

                    enableAdd();
                    new TestNet().execute();

                }


//                Log.d("MyDistanceDuration", "Iteration " + i);
//
//                String destination = mosquesArrayList.get(i).getLat() + "," + mosquesArrayList.get(i).getLng();
//                String url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + origin + "&destinations=" + destination + "&key=" + Constants.GOOGLE_MAP_DISTANCE_MATRIX_API_KEY;
//                Log.d("DistanceURL", url);
//
//                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject responseObject) {
//                        Log.i("reyjohn", responseObject.toString());
//                        try {
//
//                            JSONArray destinationArray = responseObject.getJSONArray("destination_addresses");
//                            String destinationAddress = destinationArray.getString(0);
//
//                            JSONArray originArray = responseObject.getJSONArray("origin_addresses");
//                            String originAddress = originArray.getString(0);
//
//                            JSONArray array = responseObject.getJSONArray("rows");
//                            JSONObject object = array.getJSONObject(0);
//                            JSONArray elements = object.getJSONArray("elements");
//                            JSONObject jsonObject = elements.getJSONObject(0);
//
//                            JSONObject distanceObject = jsonObject.getJSONObject("distance");
//                            String dist = distanceObject.getString("text");
//                            String[] seperated = dist.split(" ");
//                            double distanceInKM = Double.parseDouble(seperated[0]) * 1.60934;
//                            String distance = String.valueOf(round(distanceInKM, 2));
//
//                            JSONObject durationObject = jsonObject.getJSONObject("duration");
//                            String duration = durationObject.getString("text");
//
//
//                            MosqueInfo mosqueInfo = new MosqueInfo(mosquesArrayList.get(counter).getName(), distance, duration, originAddress, destinationAddress, mosquesArrayList.get(counter).getLat(), mosquesArrayList.get(counter).getLng());
//                            mosqueInfos.add(mosqueInfo);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        counter++;
//
//                        if (counter == mosquesArrayList.size() - 1) {
//
//                            enableAdd();
//                            new TestNet().execute();
//
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d("reyjohn", error.toString());
//                        Toast.makeText(getActivity(), "Network error!", Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//                // Adding request to request queue
//                AppController.getInstance().addToRequestQueue(request);

            }

        }

        private void enableAdd() {

            fullScreenAdd = new InterstitialAd(getActivity());
            fullScreenAdd.setAdUnitId(getResources().getString(R.string.test_admob_full_screen_ad_id));
            fullScreenAdRequest = new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build();
            fullScreenAdd.loadAd(fullScreenAdRequest);

            fullScreenAdd.setAdListener(new AdListener() {

                @Override
                public void onAdLoaded() {

                    Log.i("FullScreenAdd", "Loaded successfully");
                    // fullScreenAdd.show();
                    if (materialDialog.isShowing()) {
                        hidepDialog();
                    }
                    shouldShowAdd = true;

                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    Log.i("FullScreenAdd", "failed to Load");
                    if (materialDialog.isShowing()) {
                        hidepDialog();
                    }
                    shouldShowAdd = false;
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Log.i("FullScreenAdd", "Add Closed!");
                    if (isActivityRunning) {
                        setData();
                    }
                }

            });
        }


        private class TestNet extends AsyncTask<Void, Void, Void> {


            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {


                if (shouldShowAdd) {
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fullScreenAdd.show();
                                shouldShowAdd = false;
                            }
                        });
                    }
                } else {
                    if (isActivityRunning) {
                        setData();
                    }
                }


            }
        }

        private void setData() {

            hidepDialog();


            Collections.sort(mosqueInfos, new Comparator<MosqueInfo>() {

                @Override
                public int compare(MosqueInfo c1, MosqueInfo c2) {
                    return Double.compare(Double.parseDouble(c1.getMosqueDistance()), Double.parseDouble(c2.getMosqueDistance()));
                }
            });
            Log.d("MyDistanceDuration", "Adapter Called");
            mosquesListAdapter = new MosquesListAdapter(mosqueInfos, new MosquesListAdapter.SingleEntryClickListener() {
                @Override
                public void onSingleEntryClick(View v, int position) {
                    FragmentMap myMapFragment = new FragmentMap();

                    Bundle args = new Bundle();
                    args.putSerializable("MosquesInfoList", mosqueInfos);
                    args.putInt("Position", position);
                    myMapFragment.setArguments(args);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.main_activity_frameLayout, myMapFragment);
                    transaction.commit();
                }
            });

            mosques_RecycleListView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            mosques_RecycleListView.setAdapter(mosquesListAdapter);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        isActivityRunning = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActivityRunning = false;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}

