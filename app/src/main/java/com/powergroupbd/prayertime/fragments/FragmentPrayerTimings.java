package com.powergroupbd.prayertime.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.powergroupbd.prayertime.R;
import com.powergroupbd.prayertime.database.DatabaseHandler;
import com.powergroupbd.prayertime.model.Timings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Office on 2/6/2017.
 */

public class FragmentPrayerTimings extends Fragment implements View.OnClickListener {

    ImageView fajrAlarm, dhuhrAlarm, asrAlarm, maghribAlarm, ishaAlarm;
    TextView todayDate_TextView, hijriDate_TextView, fajrPrayerTime_TextView, sunriseTime_TextView, dhuhrPrayerTime_TextView, asrPrayerTime_TextView, sunsetTime_TextView, maghribPrayerTime_TextView, ishaPrayerTime_TextView;
    SharedPreferences sharedPref;

    public static FragmentPrayerTimings newInstance() {
        return new FragmentPrayerTimings();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref = getActivity().getSharedPreferences("MyPrayerTimes_SharedPreference", Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prayer_timings, container, false);

        fajrAlarm = (ImageView) view.findViewById(R.id.fajrAlarm);
        dhuhrAlarm = (ImageView) view.findViewById(R.id.dhuhrAlarm);
        asrAlarm = (ImageView) view.findViewById(R.id.asrAlarm);
        maghribAlarm = (ImageView) view.findViewById(R.id.maghribAlarm);
        ishaAlarm = (ImageView) view.findViewById(R.id.ishaAlarm);
        setAlarmStatusForPrayer();

        fajrAlarm.setOnClickListener(this);
        dhuhrAlarm.setOnClickListener(this);
        asrAlarm.setOnClickListener(this);
        maghribAlarm.setOnClickListener(this);
        ishaAlarm.setOnClickListener(this);

        todayDate_TextView = (TextView) view.findViewById(R.id.todayDate_TextView);
        hijriDate_TextView = (TextView) view.findViewById(R.id.hijriDate_TextView);
        String todayDate = getCurrentDayDate();
        todayDate_TextView.setText(todayDate);

        String hijriDate = sharedPref.getString("HijriDate", "notfound");

        if(hijriDate.equals("notfound")){
            hijriDate_TextView.setText("-- -- --");
        }
        else {
            hijriDate_TextView.setText(hijriDate);
        }

        fajrPrayerTime_TextView = (TextView) view.findViewById(R.id.fajrPrayerTime_TextView);
        sunriseTime_TextView = (TextView) view.findViewById(R.id.sunriseTime_TextView);
        dhuhrPrayerTime_TextView = (TextView) view.findViewById(R.id.dhuhrPrayerTime_TextView);
        asrPrayerTime_TextView = (TextView) view.findViewById(R.id.asrPrayerTime_TextView);
        sunsetTime_TextView = (TextView) view.findViewById(R.id.sunsetTime_TextView);
        maghribPrayerTime_TextView = (TextView) view.findViewById(R.id.maghribPrayerTime_TextView);
        ishaPrayerTime_TextView = (TextView) view.findViewById(R.id.ishaPrayerTime_TextView);


        DatabaseHandler databaseHandler = new DatabaseHandler(getActivity());

        Timings singleDayPrayerTimes = databaseHandler.getAllPrayerTimesForSingleDay(todayDate);

        String[] fajr = singleDayPrayerTimes.getFajr().split(" ");
        Log.d("Sunrise ", "sunrise  1 "+singleDayPrayerTimes.getSunrise());
        String[] sunrise = singleDayPrayerTimes.getSunrise().split(" ");
        Log.d("Sunrise ", "sunrise  2 "+sunrise[0]);
        String[] dhuhr = singleDayPrayerTimes.getDhuhr().split(" ");
        String[] asr = singleDayPrayerTimes.getAsr().split(" ");
        String[] sunset = singleDayPrayerTimes.getSunset().split(" ");
        String[] maghrib = singleDayPrayerTimes.getMaghrib().split(" ");
        String[] isha = singleDayPrayerTimes.getIsha().split(" ");

        fajrPrayerTime_TextView.setText(convertTime24To12HoursFormat(fajr[0]));
        sunriseTime_TextView.setText(convertTime24To12HoursFormat(sunrise[0]));
        dhuhrPrayerTime_TextView.setText(convertTime24To12HoursFormat(dhuhr[0]));
        asrPrayerTime_TextView.setText(convertTime24To12HoursFormat(asr[0]));
        sunsetTime_TextView.setText(convertTime24To12HoursFormat(sunset[0]));
        maghribPrayerTime_TextView.setText(convertTime24To12HoursFormat(maghrib[0]));
        ishaPrayerTime_TextView.setText(convertTime24To12HoursFormat(isha[0]));

        return view;
    }

    private String convertTime24To12HoursFormat(String time){

        String changedTime = null;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm", Locale.ENGLISH);
            final Date dateObj = sdf.parse(time);
            changedTime= new SimpleDateFormat("hh:mm a",Locale.ENGLISH).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return changedTime;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fajrAlarm:

                int fajrPrayerAlarmState = sharedPref.getInt("fajrPrayerAlarmState", 1);

                if (fajrPrayerAlarmState >= 1 && fajrPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    fajrPrayerAlarmState++;
                } else if (fajrPrayerAlarmState == 4) {
                    fajrPrayerAlarmState = 1;
                }

                if (fajrPrayerAlarmState == 1) {

                    fajrAlarm.setImageResource(R.drawable.bell_icon);
                    Toast.makeText(getActivity(), R.string.toastone, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("fajrPrayerAlarmState", 1);
                    editor.commit();

                } else if (fajrPrayerAlarmState == 2) {

                    fajrAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                    Toast.makeText(getActivity(), R.string.toasttwo, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("fajrPrayerAlarmState", 2);
                    editor.commit();

                } else if (fajrPrayerAlarmState == 3) {

                    fajrAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);
                    Toast.makeText(getActivity(), R.string.toastthree, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("fajrPrayerAlarmState", 3);
                    editor.commit();

                } else if (fajrPrayerAlarmState == 4) {

                    fajrAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                    Toast.makeText(getActivity(), R.string.toastfour, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("fajrPrayerAlarmState", 4);
                    editor.commit();

                }
                break;

            case R.id.dhuhrAlarm:

                int dhuhrPrayerAlarmState = sharedPref.getInt("dhuhrPrayerAlarmState", 1);

                if (dhuhrPrayerAlarmState >= 1 && dhuhrPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    dhuhrPrayerAlarmState++;
                } else if (dhuhrPrayerAlarmState == 4) {
                    dhuhrPrayerAlarmState = 1;
                }

                if (dhuhrPrayerAlarmState == 1) {

                    dhuhrAlarm.setImageResource(R.drawable.bell_icon);
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("dhuhrPrayerAlarmState", 1);
                    editor.commit();

                } else if (dhuhrPrayerAlarmState == 2) {

                    dhuhrAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("dhuhrPrayerAlarmState", 2);
                    editor.commit();

                } else if (dhuhrPrayerAlarmState == 3) {

                    dhuhrAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("dhuhrPrayerAlarmState", 3);
                    editor.commit();

                } else if (dhuhrPrayerAlarmState == 4) {

                    dhuhrAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("dhuhrPrayerAlarmState", 4);
                    editor.commit();

                }
                break;

            case R.id.asrAlarm:

                int asrPrayerAlarmState = sharedPref.getInt("asrPrayerAlarmState", 1);


                if (asrPrayerAlarmState >= 1 && asrPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    asrPrayerAlarmState++;
                } else if (asrPrayerAlarmState == 4) {
                    asrPrayerAlarmState = 1;
                }


                if (asrPrayerAlarmState == 1) {

                    asrAlarm.setImageResource(R.drawable.bell_icon);
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("asrPrayerAlarmState", 1);
                    editor.commit();

                } else if (asrPrayerAlarmState == 2) {

                    asrAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("asrPrayerAlarmState", 2);
                    editor.commit();

                } else if (asrPrayerAlarmState == 3) {

                    asrAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("asrPrayerAlarmState", 3);
                    editor.commit();

                } else if (asrPrayerAlarmState == 4) {

                    asrAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("asrPrayerAlarmState", 4);
                    editor.commit();

                }
                break;

            case R.id.maghribAlarm:

                int maghribPrayerAlarmState = sharedPref.getInt("maghribPrayerAlarmState", 1);

                if (maghribPrayerAlarmState >= 1 && maghribPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    maghribPrayerAlarmState++;
                } else if (maghribPrayerAlarmState == 4) {
                    maghribPrayerAlarmState = 1;
                }

                if (maghribPrayerAlarmState == 1) {

                    maghribAlarm.setImageResource(R.drawable.bell_icon);
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("maghribPrayerAlarmState", 1);
                    editor.commit();

                } else if (maghribPrayerAlarmState == 2) {

                    maghribAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("maghribPrayerAlarmState", 2);
                    editor.commit();

                } else if (maghribPrayerAlarmState == 3) {

                    maghribAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("maghribPrayerAlarmState", 3);
                    editor.commit();

                } else if (maghribPrayerAlarmState == 4) {

                    maghribAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("maghribPrayerAlarmState", 4);
                    editor.commit();

                }
                break;

            case R.id.ishaAlarm:

                int ishaPrayerAlarmState = sharedPref.getInt("ishaPrayerAlarmState", 1);

                if (ishaPrayerAlarmState >= 1 && ishaPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    ishaPrayerAlarmState++;
                } else if (ishaPrayerAlarmState == 4) {
                    ishaPrayerAlarmState = 1;
                }


                if (ishaPrayerAlarmState == 1) {

                    ishaAlarm.setImageResource(R.drawable.bell_icon);
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("ishaPrayerAlarmState", 1);
                    editor.commit();

                } else if (ishaPrayerAlarmState == 2) {

                    ishaAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("ishaPrayerAlarmState", 2);
                    editor.commit();

                } else if (ishaPrayerAlarmState == 3) {

                    ishaAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("ishaPrayerAlarmState", 3);
                    editor.commit();

                } else if (ishaPrayerAlarmState == 4) {

                    ishaAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("ishaPrayerAlarmState", 4);
                    editor.commit();

                }
                break;
        }
    }

    public String getCurrentDayDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        String currentDate = sdf.format(cal.getTime());

        Log.d("CurrentDate", currentDate);
        return currentDate;
    }

    private void setAlarmStatusForPrayer() {

            int fajrPrayerAlarmState = sharedPref.getInt("fajrPrayerAlarmState", 1);

            if (fajrPrayerAlarmState == 1) {

                fajrAlarm.setImageResource(R.drawable.bell_icon);

            } else if (fajrPrayerAlarmState == 2) {

                fajrAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);

            } else if (fajrPrayerAlarmState == 3) {

                fajrAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);

            } else if (fajrPrayerAlarmState == 4) {

                fajrAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
            }


            int dhuhrPrayerAlarmState = sharedPref.getInt("dhuhrPrayerAlarmState", 1);

            if (dhuhrPrayerAlarmState == 1) {

                dhuhrAlarm.setImageResource(R.drawable.bell_icon);

            } else if (dhuhrPrayerAlarmState == 2) {

                dhuhrAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);

            } else if (dhuhrPrayerAlarmState == 3) {

                dhuhrAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);

            } else if (dhuhrPrayerAlarmState == 4) {

                dhuhrAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
            }

            int asrPrayerAlarmState = sharedPref.getInt("asrPrayerAlarmState", 1);


            if (asrPrayerAlarmState == 1) {

                asrAlarm.setImageResource(R.drawable.bell_icon);

            } else if (asrPrayerAlarmState == 2) {

                asrAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);

            } else if (asrPrayerAlarmState == 3) {

                asrAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);

            } else if (asrPrayerAlarmState == 4) {

                asrAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
            }


            int maghribPrayerAlarmState = sharedPref.getInt("maghribPrayerAlarmState", 1);


            if (maghribPrayerAlarmState == 1) {

                maghribAlarm.setImageResource(R.drawable.bell_icon);

            } else if (maghribPrayerAlarmState == 2) {

                maghribAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);

            } else if (maghribPrayerAlarmState == 3) {

                maghribAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);

            } else if (maghribPrayerAlarmState == 4) {

                maghribAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
            }

            int ishaPrayerAlarmState = sharedPref.getInt("ishaPrayerAlarmState", 1);

            if (ishaPrayerAlarmState == 1) {

                ishaAlarm.setImageResource(R.drawable.bell_icon);

            } else if (ishaPrayerAlarmState == 2) {

                ishaAlarm.setImageResource(R.drawable.ic_alarm_on_white_48dp);

            } else if (ishaPrayerAlarmState == 3) {

                ishaAlarm.setImageResource(R.drawable.ic_snooze_white_48dp);

            } else if (ishaPrayerAlarmState == 4) {

                ishaAlarm.setImageResource(R.drawable.ic_alarm_off_white_48dp);
            }

        }

    }
