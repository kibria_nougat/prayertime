package com.powergroupbd.prayertime.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.powergroupbd.prayertime.AlarmSetter;
import com.powergroupbd.prayertime.R;
import com.powergroupbd.prayertime.database.DatabaseHandler;
import com.powergroupbd.prayertime.model.Timings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Office on 2/6/2017.
 */

public class FragmentHome extends Fragment implements View.OnClickListener {


    TextView prayerName_TextView, prayerTime_TextView, countDown_TextView, prayerDate_TextView, cityName_TextView, tomorrow_TextView;
    ImageView alarmBell_ImageView, shareImageView, homeLayoutBackground_ImageView;
    Context mContext;
    SharedPreferences sharedPref;
    private static String currentCityName;
    DatabaseHandler databaseHandler;
    Timings singleDayPrayerTimes;

    public static FragmentHome newInstance() {
        return new FragmentHome();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
        sharedPref = mContext.getSharedPreferences("MyPrayerTimes_SharedPreference", Context.MODE_PRIVATE);
        currentCityName = sharedPref.getString("MyCurrentCityName", "");
        databaseHandler = new DatabaseHandler(getActivity());

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("IS_APP_RUNNING_FIRST", 1); // 1 means app already ran for first time;
        editor.commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.framgment_home, container, false);

        prayerName_TextView = (TextView) view.findViewById(R.id.prayerName_TextView);
        prayerTime_TextView = (TextView) view.findViewById(R.id.prayerTime_TextView);
        countDown_TextView = (TextView) view.findViewById(R.id.countDown_TextView);
        prayerDate_TextView = (TextView) view.findViewById(R.id.prayerDate_TextView);
        cityName_TextView = (TextView) view.findViewById(R.id.cityName_TextView);
        tomorrow_TextView = (TextView) view.findViewById(R.id.tomorrow_TextView);
        alarmBell_ImageView = (ImageView) view.findViewById(R.id.alarmBell_ImageView);
        shareImageView = (ImageView) view.findViewById(R.id.shareImageView);
        homeLayoutBackground_ImageView = (ImageView) view.findViewById(R.id.homeLayoutBackground_ImageView);


        shareImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String myText = getResources().getString(R.string.app_name) + "\n" + "https://play.google.com/store/apps/details?id=com.powergroupbd.prayertime";
                sharingIntent.putExtra(Intent.EXTRA_TEXT, myText);
                startActivity(Intent.createChooser(sharingIntent, ""));
            }
        });

        if (currentCityName.equals("")) {
            cityName_TextView.setText(R.string.cityname);
        } else {
            cityName_TextView.setText(currentCityName);
        }

        alarmBell_ImageView.setOnClickListener(this);

        setPrayerTimes(getCurrentDayDate());

        setAlarmStatusForPrayer(0);

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void setPrayerTimes(String date) {

        singleDayPrayerTimes = databaseHandler.getAllPrayerTimesForSingleDay(date);

        SimpleDateFormat msdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        String timeZone = sharedPref.getString("MyTimeZone", "");
        Log.d("TimeZoneTest", timeZone);
        msdf.setTimeZone(TimeZone.getTimeZone(timeZone));

        int has_database_been_updated = sharedPref.getInt("HAS_DATABASE_BEEN_UPGRADED", 0);

        if (has_database_been_updated == 1) {
            AlarmSetter alarmSetter = new AlarmSetter();
            alarmSetter.setAlarm(getActivity(), singleDayPrayerTimes.getFajr(), singleDayPrayerTimes.getDhuhr(), singleDayPrayerTimes.getAsr(), singleDayPrayerTimes.getMaghrib(), singleDayPrayerTimes.getIsha());

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("HAS_DATABASE_BEEN_UPGRADED", 2); // 2 means database table data needs to be updated deleting the previous table data;
            editor.commit();
        }

        long currentTime = getCurrentTimeInMillis();

        try {

            if (singleDayPrayerTimes != null) {

                Date fajr = msdf.parse(singleDayPrayerTimes.getFajr());
                Date dhuhr = msdf.parse(singleDayPrayerTimes.getDhuhr());
                Date asr = msdf.parse(singleDayPrayerTimes.getAsr());
                Date maghrib = msdf.parse(singleDayPrayerTimes.getMaghrib());
                Date isha = msdf.parse(singleDayPrayerTimes.getIsha());

                // this condition will check which is the next prayer time and then set the time and count down timer;

                if (fajr.getTime() > currentTime) {

//                    homeLayoutBackground_ImageView.setImageResource(R.drawable.fajar_prayer_image);
                    prayerName_TextView.setText(R.string.fajr);
                    String[] name = singleDayPrayerTimes.getFajr().split(" ");

                    prayerTime_TextView.setText(convertTime24To12HoursFormat(name[0]));
                    prayerDate_TextView.setText(singleDayPrayerTimes.getDate());

                    long timeDifferenceInMiliSeconds = fajr.getTime() - getCurrentTimeInMillis();
                    Log.d("TimeInMilis", timeDifferenceInMiliSeconds + "");
                    MyCountDownTimer myCountDownTimer = new MyCountDownTimer(timeDifferenceInMiliSeconds, 1000, singleDayPrayerTimes);
                    myCountDownTimer.start();


                } else if (dhuhr.getTime() > currentTime) {

//                    homeLayoutBackground_ImageView.setImageResource(R.drawable.dhuhr_prayer_image);
                    prayerName_TextView.setText(R.string.dhuhr);
                    String[] name = singleDayPrayerTimes.getDhuhr().split(" ");
                    prayerTime_TextView.setText(convertTime24To12HoursFormat(name[0]));
                    prayerDate_TextView.setText(singleDayPrayerTimes.getDate());

                    long timeDifferenceInMiliSeconds = dhuhr.getTime() - getCurrentTimeInMillis();
                    Log.d("TimeInMilis", timeDifferenceInMiliSeconds + "");
                    MyCountDownTimer myCountDownTimer = new MyCountDownTimer(timeDifferenceInMiliSeconds, 1000, singleDayPrayerTimes);
                    myCountDownTimer.start();


                } else if (asr.getTime() > currentTime) {

//                    homeLayoutBackground_ImageView.setImageResource(R.drawable.asr_prayer_image);
                    prayerName_TextView.setText(R.string.asr);
                    String[] name = singleDayPrayerTimes.getAsr().split(" ");
                    prayerTime_TextView.setText(convertTime24To12HoursFormat(name[0]));
                    prayerDate_TextView.setText(singleDayPrayerTimes.getDate());

                    long timeDifferenceInMiliSeconds = asr.getTime() - getCurrentTimeInMillis();
                    Log.d("TimeInMilis", timeDifferenceInMiliSeconds + "");
                    MyCountDownTimer myCountDownTimer = new MyCountDownTimer(timeDifferenceInMiliSeconds, 1000, singleDayPrayerTimes);
                    myCountDownTimer.start();

                } else if (maghrib.getTime() > currentTime) {

//                    homeLayoutBackground_ImageView.setImageResource(R.drawable.maghrib_prayer_image);
                    prayerName_TextView.setText(R.string.maghrib);
                    String[] name = singleDayPrayerTimes.getMaghrib().split(" ");
                    prayerTime_TextView.setText(convertTime24To12HoursFormat(name[0]));
                    prayerDate_TextView.setText(singleDayPrayerTimes.getDate());

                    long timeDifferenceInMiliSeconds = maghrib.getTime() - getCurrentTimeInMillis();
                    Log.d("TimeInMilis", timeDifferenceInMiliSeconds + "");
                    MyCountDownTimer myCountDownTimer = new MyCountDownTimer(timeDifferenceInMiliSeconds, 1000, singleDayPrayerTimes);
                    myCountDownTimer.start();

                } else if (isha.getTime() > currentTime) {

//                    homeLayoutBackground_ImageView.setImageResource(R.drawable.isha_prayer_image);
                    prayerName_TextView.setText(R.string.isha);
                    String[] name = singleDayPrayerTimes.getIsha().split(" ");
                    prayerTime_TextView.setText(convertTime24To12HoursFormat(name[0]));
                    prayerDate_TextView.setText(singleDayPrayerTimes.getDate());

                    long timeDifferenceInMiliSeconds = isha.getTime() - getCurrentTimeInMillis();
                    Log.d("TimeInMilis", timeDifferenceInMiliSeconds + "");
                    MyCountDownTimer myCountDownTimer = new MyCountDownTimer(timeDifferenceInMiliSeconds, 1000, singleDayPrayerTimes);
                    myCountDownTimer.start();

                } else if (currentTime >= isha.getTime()) {

//                    homeLayoutBackground_ImageView.setImageResource(R.drawable.fajar_prayer_image);
                    tomorrow_TextView.setVisibility(View.VISIBLE);
                    Timings singleDayPrayerTimesForNextDay = databaseHandler.getAllPrayerTimesForSingleDay(getNextDayDate());
                    prayerName_TextView.setText(R.string.fajr);
                    String[] name = singleDayPrayerTimesForNextDay.getFajr().split(" ");
                    prayerTime_TextView.setText(convertTime24To12HoursFormat(name[0]));
                    prayerDate_TextView.setText(singleDayPrayerTimes.getDate());

                    Date fajrNextDay = msdf.parse(singleDayPrayerTimesForNextDay.getFajr());

                    long timeDifferenceInMiliSeconds = 86400000 - getCurrentTimeInMillis() + fajrNextDay.getTime();
                    Log.d("NExtdayFajr", "Nextday fajr " + fajrNextDay.getTime());
                    Log.d("TimeInMilis", timeDifferenceInMiliSeconds + "");
                    MyCountDownTimer myCountDownTimer = new MyCountDownTimer(timeDifferenceInMiliSeconds, 1000, singleDayPrayerTimes);
                    myCountDownTimer.start();
                }

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    public String getCurrentDayDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        String currentDate = sdf.format(cal.getTime());

        Log.d("CurrentDate", currentDate);
        return currentDate;
    }

    public String getNextDayDate() {

        String nextDay;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        String currentDate = sdf.format(cal.getTime());

        String[] separated = currentDate.split(" ");
        String currentMonth = separated[1];

        int totalNumberOfDaysInCurrentMonth = databaseHandler.getNumberOFDaysInMonth(currentMonth);

        int today_date = Integer.parseInt(separated[0]);
        String month = separated[1];
        String year = separated[2];
        int nextday_date = today_date + 1;

        if (nextday_date <= totalNumberOfDaysInCurrentMonth) {

            String tomorrowDate = String.valueOf(nextday_date);
            nextDay = tomorrowDate + " " + month + " " + year;
            Log.d("NEXTDAY", nextDay);
            return getFormattedDate(nextDay, "d MMM yyyy", "dd MMM yyyy");
        } else {

            Calendar c = Calendar.getInstance();
            int year_temp = c.get(Calendar.YEAR);
            int month_temp = c.get(Calendar.MONTH) + 1;
            int nextMonth;
            if (month_temp == 12) {
                nextMonth = 0;
                year_temp = year_temp + 1;
            } else {
                nextMonth = month_temp;
            }

            c.set(Calendar.YEAR, year_temp);
            c.set(Calendar.MONTH, nextMonth);
            c.set(Calendar.DATE, 1);

            String nextDayDate = sdf.format(c.getTime());
            String[] separatedString = nextDayDate.split(" ");

            String tomorrowDate = separatedString[0];
            String tomorrowMonth = separatedString[1];
            String yearForNextMonth = separatedString[2];
            nextDay = tomorrowDate + " " + tomorrowMonth + " " + yearForNextMonth;
            Log.d("NEXTDAY", nextDay);

            return getFormattedDate(nextDay, "d MMM yyyy", "dd MMM yyyy");
        }
    }

    public static String getFormattedDate(String givenDate, String fromFormat, String expectedFormat) {

        String strCurrentDate = givenDate;
        SimpleDateFormat format = new SimpleDateFormat(fromFormat, Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat(expectedFormat, Locale.ENGLISH);
        return format.format(newDate);


    }

    public long getCurrentTimeInMillis() {
        Calendar c = Calendar.getInstance();
        long noW = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long currentTime = noW - c.getTimeInMillis();
        return currentTime;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.alarmBell_ImageView) {

            int fromOnClick = 1;
            setAlarmStatusForPrayer(fromOnClick);
        }

    }

    private void setAlarmStatusForPrayer(int isFromOnClick) {

        String currentPrayerName = prayerName_TextView.getText().toString();

        if (currentPrayerName.equals("Fajr")) {

            int fajrPrayerAlarmState = sharedPref.getInt("fajrPrayerAlarmState", 1);

            if (isFromOnClick == 1) {
                if (fajrPrayerAlarmState >= 1 && fajrPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    fajrPrayerAlarmState++;
                } else if (fajrPrayerAlarmState == 4) {
                    fajrPrayerAlarmState = 1;
                }
            }

            if (fajrPrayerAlarmState == 1) {

                alarmBell_ImageView.setImageResource(R.drawable.bell_icon);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), R.string.toastone, Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("fajrPrayerAlarmState", 1);
                editor.commit();

            } else if (fajrPrayerAlarmState == 2) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), R.string.toasttwo, Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("fajrPrayerAlarmState", 2);
                editor.commit();

            } else if (fajrPrayerAlarmState == 3) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_snooze_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), R.string.toastthree, Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("fajrPrayerAlarmState", 3);
                editor.commit();

            } else if (fajrPrayerAlarmState == 4) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), R.string.toastfour, Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("fajrPrayerAlarmState", 4);
                editor.commit();

            }

        } else if (currentPrayerName.equals("Dhuhr")) {

            int dhuhrPrayerAlarmState = sharedPref.getInt("dhuhrPrayerAlarmState", 1);

            if (isFromOnClick == 1) {
                if (dhuhrPrayerAlarmState >= 1 && dhuhrPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    dhuhrPrayerAlarmState++;
                } else if (dhuhrPrayerAlarmState == 4) {
                    dhuhrPrayerAlarmState = 1;
                }
            }

            if (dhuhrPrayerAlarmState == 1) {

                alarmBell_ImageView.setImageResource(R.drawable.bell_icon);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("dhuhrPrayerAlarmState", 1);
                editor.commit();

            } else if (dhuhrPrayerAlarmState == 2) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("dhuhrPrayerAlarmState", 2);
                editor.commit();

            } else if (dhuhrPrayerAlarmState == 3) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_snooze_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("dhuhrPrayerAlarmState", 3);
                editor.commit();

            } else if (dhuhrPrayerAlarmState == 4) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("dhuhrPrayerAlarmState", 4);
                editor.commit();

            }

        } else if (currentPrayerName.equals("Asr")) {

            int asrPrayerAlarmState = sharedPref.getInt("asrPrayerAlarmState", 1);

            if (isFromOnClick == 1) {
                if (asrPrayerAlarmState >= 1 && asrPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    asrPrayerAlarmState++;
                } else if (asrPrayerAlarmState == 4) {
                    asrPrayerAlarmState = 1;
                }
            }

            if (asrPrayerAlarmState == 1) {

                alarmBell_ImageView.setImageResource(R.drawable.bell_icon);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("asrPrayerAlarmState", 1);
                editor.commit();

            } else if (asrPrayerAlarmState == 2) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("asrPrayerAlarmState", 2);
                editor.commit();

            } else if (asrPrayerAlarmState == 3) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_snooze_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("asrPrayerAlarmState", 3);
                editor.commit();

            } else if (asrPrayerAlarmState == 4) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("asrPrayerAlarmState", 4);
                editor.commit();

            }

        } else if (currentPrayerName.equals("Maghrib")) {

            int maghribPrayerAlarmState = sharedPref.getInt("maghribPrayerAlarmState", 1);

            if (isFromOnClick == 1) {
                if (maghribPrayerAlarmState >= 1 && maghribPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    maghribPrayerAlarmState++;
                } else if (maghribPrayerAlarmState == 4) {
                    maghribPrayerAlarmState = 1;
                }
            }

            if (maghribPrayerAlarmState == 1) {

                alarmBell_ImageView.setImageResource(R.drawable.bell_icon);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("maghribPrayerAlarmState", 1);
                editor.commit();

            } else if (maghribPrayerAlarmState == 2) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("maghribPrayerAlarmState", 2);
                editor.commit();

            } else if (maghribPrayerAlarmState == 3) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_snooze_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("maghribPrayerAlarmState", 3);
                editor.commit();

            } else if (maghribPrayerAlarmState == 4) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("maghribPrayerAlarmState", 4);
                editor.commit();

            }

        } else if (currentPrayerName.equals("Isha")) {

            int ishaPrayerAlarmState = sharedPref.getInt("ishaPrayerAlarmState", 1);

            if (isFromOnClick == 1) {
                if (ishaPrayerAlarmState >= 1 && ishaPrayerAlarmState < 4) { // for iterating 1 to 4 and if value becomes 4 then jump to 1 again;
                    ishaPrayerAlarmState++;
                } else if (ishaPrayerAlarmState == 4) {
                    ishaPrayerAlarmState = 1;
                }
            }

            if (ishaPrayerAlarmState == 1) {

                alarmBell_ImageView.setImageResource(R.drawable.bell_icon);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play Adhan!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("ishaPrayerAlarmState", 1);
                editor.commit();

            } else if (ishaPrayerAlarmState == 2) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_on_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will play default ringtone!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("ishaPrayerAlarmState", 2);
                editor.commit();

            } else if (ishaPrayerAlarmState == 3) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_snooze_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Alarm will show notification but no sound!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("ishaPrayerAlarmState", 3);
                editor.commit();

            } else if (ishaPrayerAlarmState == 4) {

                alarmBell_ImageView.setImageResource(R.drawable.ic_alarm_off_white_48dp);
                if (isFromOnClick == 1)
                    Toast.makeText(getActivity(), "Nothing will show!", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("ishaPrayerAlarmState", 4);
                editor.commit();

            }

        }

    }

    //** for counting down time

    class MyCountDownTimer extends CountDownTimer {

        Timings singleDayPrayerTimes;

        public MyCountDownTimer(long millisInFuture, long interval, Timings singleDayPrayerTimes) {
            super(millisInFuture, interval);
            this.singleDayPrayerTimes = singleDayPrayerTimes;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int seconds = (int) (millisUntilFinished / 1000) % 60;
            int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
            int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);

            String second = "00", minute = "00", hour = "00";

            long timeDifferenceInMiliSeconds = 86400000 - getCurrentTimeInMillis();
            Log.d("MyCurrentTimeInMillis", "Current Time " + getCurrentTimeInMillis() + "difference " + timeDifferenceInMiliSeconds);

            if (timeDifferenceInMiliSeconds <= 1000 && timeDifferenceInMiliSeconds >= 0) {
                tomorrow_TextView.setVisibility(View.GONE);
            }

            if (seconds < 10) {
                second = "0" + String.valueOf(seconds);
            } else {
                second = String.valueOf(seconds);
            }

            if (minutes < 10) {
                minute = "0" + String.valueOf(minutes);
            } else {
                minute = String.valueOf(minutes);
            }

            if (hours < 10) {
                hour = "0" + String.valueOf(hours);
            } else {
                hour = String.valueOf(hours);
            }

            countDown_TextView.setText("(" + hour + ":" + minute + ":" + second + ")");

        }

        @Override
        public void onFinish() {

            countDown_TextView.setText("00:00:00");

            if (prayerName_TextView.getText().toString().equals("Isha")) {
                setPrayerTimes(getNextDayDate());
            } else {
                setPrayerTimes(getCurrentDayDate());
            }

            setAlarmStatusForPrayer(0);

        }
    }

    private String convertTime24To12HoursFormat(String time) {

        String changedTime = null;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm", Locale.ENGLISH);
            final Date dateObj = sdf.parse(time);
            changedTime = new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return changedTime;
    }

}
