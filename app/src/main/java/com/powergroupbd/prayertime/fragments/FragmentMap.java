package com.powergroupbd.prayertime.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.powergroupbd.prayertime.DirectionFinder;
import com.powergroupbd.prayertime.DirectionFinderListener;
import com.powergroupbd.prayertime.R;
import com.powergroupbd.prayertime.model.MosqueInfo;
import com.powergroupbd.prayertime.model.Mosques;
import com.powergroupbd.prayertime.model.Route;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Office on 2/15/2017.
 */

public class FragmentMap extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, com.google.android.gms.location.LocationListener, DirectionFinderListener {

    private GoogleMap mMap;
    ArrayList<MosqueInfo> mosquesInfoList;
    MapView mMapView;
    GoogleApiClient mGoogleApiClient = null;
    Location mLastLocation = null;
    LocationRequest mLocationRequest;
    LatLng latLng;
    Marker currLocationMarker;
    static double latitude = 0;
    static double longitude = 0;
    MaterialDialog materialDialog;
    static String mosqueName = null, destinationAddress = null;
    TextView mosqueName_TextView, destinationTextView, distanceTextView;

    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    int position = -1;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        Log.d("MosquesList", "OnCreateCalled");
        mosquesInfoList = (ArrayList<MosqueInfo>) getArguments().getSerializable("MosquesInfoList");
        position = getArguments().getInt("Position");
        Log.d("MosquesList", "MosquesList size " + mosquesInfoList.size());
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);
        mMapView = (MapView) view.findViewById(R.id.googleMap);
        ImageView backIcon_ImageView = (ImageView) view.findViewById(R.id.backIcon_ImageView);
        mosqueName_TextView = (TextView) view.findViewById(R.id.mosqueName_TextView);
        destinationTextView = (TextView) view.findViewById(R.id.destinationTextView);
        distanceTextView = (TextView) view.findViewById(R.id.distanceTextView);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMapView.getMapAsync(this);

        backIcon_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_activity_frameLayout, FragmentMosques.newInstance());
                transaction.commit();
            }
        });

        return view;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d("MosquesList", "OnMapReadyCalled");
        mMap = googleMap;
        mMap.setPadding(0, 95, 0, 0);
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.

            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.map_style));

            if (success) {
                mMap.setMyLocationEnabled(true);
                buildGoogleApiClient();
                setMarkersForAvailableMosques(mosquesInfoList);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        // Position the map's camera near Sydney, Australia.
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-34, 151)));
    }


    private void setMarkersForAvailableMosques(final ArrayList<MosqueInfo> mosquesInfoArrayList) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.mosque_marker_icon);
        Log.d("MosquesList", "MosquesArrayList size" + mosquesInfoArrayList.size());
        mMap.clear();
        for (int i = 0; i < mosquesInfoArrayList.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();

            double lat = Double.parseDouble(mosquesInfoArrayList.get(i).getLat());
            double lng = Double.parseDouble(mosquesInfoArrayList.get(i).getLng());

            Log.d("GoogleMap ", "Lat: " + lat + " Lng: " + lng);
            String placeName = mosquesInfoArrayList.get(i).getMosqueName();
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);
            markerOptions.title(placeName);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bm));
            mMap.addMarker(markerOptions);
        }

        if (position > -1) {

            mosqueName = mosquesInfoArrayList.get(position).getMosqueName();
            String end_Latitude = String.valueOf(mosquesInfoArrayList.get(position).getLat());
            String end_Longitude = String.valueOf(mosquesInfoArrayList.get(position).getLng());
            String end_Position = end_Latitude + "," + end_Longitude;
            sendRequest(end_Position);
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() { // to draw a direction line when a marker will be touched;
            @Override
            public boolean onMarkerClick(Marker marker) {

                LatLng end_position = marker.getPosition();
                Double end_latitude = end_position.latitude;
                Double end_longitude = end_position.longitude;

                String end_Latitude = String.valueOf(end_latitude);
                String end_Longitude = String.valueOf(end_longitude);

                String end_Position = end_Latitude + "," + end_Longitude;

                for (int i = 0; i < mosquesInfoArrayList.size(); i++) {
                    if (mosquesInfoArrayList.get(i).getLat().equals(end_Latitude) && mosquesInfoArrayList.get(i).getLng().equals(end_Longitude)) {
                        mosqueName = mosquesInfoArrayList.get(i).getMosqueName();
                        destinationAddress = mosquesInfoArrayList.get(i).getDestinationAddress();
                    }
                }

                sendRequest(end_Position);

                return true;
            }
        });

    }


    @Override
    public void onLocationChanged(Location location) {

        if (currLocationMarker != null) {
            currLocationMarker.remove();
        }
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);

        //Toast.makeText(this,"Location Changed",Toast.LENGTH_SHORT).show();
        //zoom to current position:
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }

    protected synchronized void buildGoogleApiClient() {
        //Toast.makeText(this, "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.d("OnConnected", "OnConnectedCalled");
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            //place marker at current position
            //mGoogleMap.clear();

            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");

            currLocationMarker = mMap.addMarker(markerOptions);

            CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }


    private void sendRequest(String end_position) {
        String origin_Latitude = String.valueOf(latitude);
        String origin_Longitude = String.valueOf(longitude);
        String origin = origin_Latitude + "," + origin_Longitude;
        String destination = end_position.toString();
        //Toast.makeText(MapsActivity.this, "Origin = "+origin+" Destination "+destination, Toast.LENGTH_LONG).show();

        if (origin.isEmpty()) {
            Toast.makeText(getActivity(), "Origin location not found!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destination.isEmpty()) {
            Toast.makeText(getActivity(), "Destination location not found!", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onDirectionFinderStart() {

        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.findingdirection)
                .content(R.string.pleasewait)
                .progress(true, 0)
                .titleColor(Color.parseColor("#263238"))
                .backgroundColor(Color.parseColor("#ffffff"))
                .contentColor(Color.parseColor("#37474f"))
                .cancelable(true)
                .widgetColor(Color.parseColor("#1d8049"))
                .show();

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    // for implementing own(DirectionFinderListener) listener class;
    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        materialDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            String duration = (route.duration.text);
            String distance = (route.distance.text);

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.parseColor("#1d8049")).
                    width(12);


            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));

            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            mMap.animateCamera(zoom);

            mosqueName_TextView.setText("Name: " + mosqueName);
            destinationTextView.setText("Address: " + destinationAddress);
            distanceTextView.setText("Distance: " + distance + "  Duration: " + duration);
        }
    }
}
