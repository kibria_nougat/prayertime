package com.powergroupbd.prayertime;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.powergroupbd.prayertime.database.DatabaseHandler;
import com.powergroupbd.prayertime.model.Timings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Office on 2/7/2017.
 */

public class AlarmReceiverService extends Service {

    SharedPreferences sharedPref;
    Ringtone r;

    public AlarmReceiverService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("ServiceStatus", "ServiceStarted");
        sharedPref = getApplicationContext().getSharedPreferences("MyPrayerTimes_SharedPreference", Context.MODE_PRIVATE);


        if (intent.getExtras() != null) {

            if (intent.getExtras().getString("PrayerName") != null) {
                String prayerName = intent.getExtras().getString("PrayerName");

                if (prayerName.equals("Fajr")) {

                    int state = sharedPref.getInt("fajrPrayerAlarmState", 1);
                    playAlarmAccordingToUserSettings("Fajr", state , 1); // 1 is for playing fajr adhan;

                } else if (prayerName.equals("Dhuhr")) {

                    int state = sharedPref.getInt("dhuhrPrayerAlarmState", 1);
                    playAlarmAccordingToUserSettings("Dhuhr", state , 0);

                } else if (prayerName.equals("Asr")) {

                    int state = sharedPref.getInt("asrPrayerAlarmState", 1);
                    playAlarmAccordingToUserSettings("Asr", state, 0);

                } else if (prayerName.equals("Maghrib")) {

                    int state = sharedPref.getInt("maghribPrayerAlarmState", 1);
                    playAlarmAccordingToUserSettings("Maghrib", state, 0);

                } else if (prayerName.equals("Isha")) {

                    int state = sharedPref.getInt("ishaPrayerAlarmState", 1);
                    playAlarmAccordingToUserSettings("Isha", state, 0);


                } else if (prayerName.equals("NewDay")) {

                    setAlarmTimesForNextDay();

                }

            }


        }

        return Service.START_NOT_STICKY;
    }

    public void setAlarmTimesForNextDay() {

        String nextDay = null;

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String currentDate = sdf.format(cal.getTime());
        String[] separated = currentDate.split(" ");

        String currentMonth = separated[1];

        DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
        int totalNumberOfDaysInCuttentMonth = databaseHandler.getNumberOFDaysInMonth(currentMonth);

        int today_date = Integer.parseInt(separated[0]);
        String month = separated[1];
        String year = separated[2];
        int nextday_date = today_date;

        if (nextday_date <= totalNumberOfDaysInCuttentMonth) {
            String tomorrowDate = String.valueOf(nextday_date);
            nextDay = tomorrowDate + " " + month + " " + year;
            Log.d("NEXTDAY", nextDay);
        }

        Timings singleDayPrayerTimes = databaseHandler.getAllPrayerTimesForSingleDay(nextDay);
        Log.d("NEXTDAY", nextDay + " Fajr " + singleDayPrayerTimes.getFajr());

        if (singleDayPrayerTimes != null) {
            AlarmSetter alarmSetter = new AlarmSetter();
            alarmSetter.setAlarm(getApplicationContext(), singleDayPrayerTimes.getFajr(), singleDayPrayerTimes.getDhuhr(), singleDayPrayerTimes.getAsr(), singleDayPrayerTimes.getMaghrib(), singleDayPrayerTimes.getIsha());
        }

    }


    private void playAlarmAccordingToUserSettings(String prayerName, int state ,int should_fajr_adhan_play){

        if (state == 1) {

            Intent notificationIntent = new Intent(getApplicationContext(), AdhanNotificationActivity.class);
            notificationIntent.putExtra("prayer_name", prayerName);
            notificationIntent.putExtra("should_fajr_adhan_play", should_fajr_adhan_play);
            notificationIntent.putExtra("should_play_adhan_or_default_alarm", 1);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(notificationIntent);

        } else if (state == 2) {

            Intent notificationIntent = new Intent(getApplicationContext(), AdhanNotificationActivity.class);
            notificationIntent.putExtra("prayer_name", prayerName);
            notificationIntent.putExtra("should_fajr_adhan_play", should_fajr_adhan_play);
            notificationIntent.putExtra("should_play_adhan_or_default_alarm", 0);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(notificationIntent);

        } else if (state == 3) {

            Intent notificationIntent = new Intent(getApplicationContext(), AdhanNotificationActivity.class);
            notificationIntent.putExtra("prayer_name", prayerName);
            notificationIntent.putExtra("should_fajr_adhan_play", 222);
            notificationIntent.putExtra("should_play_adhan_or_default_alarm", 333);
            notificationIntent.putExtra("Is_play_nothing", 1);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(notificationIntent);

        } else {

        }
    }

}
