package com.powergroupbd.prayertime.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.powergroupbd.prayertime.model.Timings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Office on 2/7/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "PRAYER_TIMES_DATABASE";
    //  table names
    private static final String TABLE_PRAYER_TIME = "prayer_times";

    Context mContext;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }


    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_PRAYER_TIMES_TABLE = "CREATE TABLE prayer_times (" +
                " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                " fajr VARCHAR," +
                " sunrise VARCHAR," +
                " dhuar VARCHAR," +
                " asr VARCHAR," +
                " sunset VARCHAR," +
                " maghrib VARCHAR," +
                " isha VARCHAR," +
                " date VARCHAR," +
                " month VARCHAR);";
        db.execSQL(CREATE_PRAYER_TIMES_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRAYER_TIME);
        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    public void deleteAllDataFromTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_PRAYER_TIME);
        db.close();
    }


    // Getting all prayer times of a single day;
    public Timings getAllPrayerTimesForSingleDay(String expected_date) {

        Log.d("GetALLPrayerTimes", "called"+" Expected date "+expected_date);
        Timings singleDayPrayerTimes = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM prayer_times where date = ?; ", new String[] {expected_date});

        if (cursor.moveToFirst()) {
            do {
                String fajr = cursor.getString(1);
                String sunrise = cursor.getString(2);
                String dhuar = cursor.getString(3);
                String asr = cursor.getString(4);
                String sunset = cursor.getString(5);
                String maghrib = cursor.getString(6);
                String isha = cursor.getString(7);
                String date = cursor.getString(8);
                String month = cursor.getString(9);

                Log.d("MyTimings", "fajr "+fajr+" sunrise "+sunrise+" dhuar "+dhuar+" asr "+asr+" sunset "+sunset+" maghrib "+maghrib+" isha "+isha+" date "+date+ " month "+month);
                singleDayPrayerTimes = new Timings(fajr, sunrise, dhuar, asr, sunset, maghrib, isha, date, month);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return singleDayPrayerTimes;
    }

    public int getNumberOFDaysInMonth(String month){

        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT  * FROM  prayer_times where month = '"+month+"'";
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        Log.d("NumberOFRow", "Row : "+cnt);
        cursor.close();
        db.close();
        return cnt;

    }



    public void insertPrayerTimesIntoDatabase(ArrayList<Timings> prayerTimeList){

        SQLiteDatabase db = this.getWritableDatabase();

        if(db == null){
            Log.d("MyTimings", "Null Database"+"  YES");
        }
        else {
            Log.d("MyTimings", "Not Null Database"+"  YES");
        }

        for(int i=0;i<prayerTimeList.size();i++){

            String fajr = prayerTimeList.get(i).getFajr();
            String sunrise = prayerTimeList.get(i).getSunrise();
            String dhuar = prayerTimeList.get(i).getDhuhr();
            String asr = prayerTimeList.get(i).getAsr();
            String sunset = prayerTimeList.get(i).getSunset();
            String maghrib = prayerTimeList.get(i).getMaghrib();
            String isha = prayerTimeList.get(i).getIsha();
            String date = prayerTimeList.get(i).getDate();
            String month = prayerTimeList.get(i).getMonth();
            String query;

//            if(i==23){
//                query = "INSERT INTO prayer_times (fajr, sunrise, dhuar, asr, sunset, maghrib, isha, date, month) VALUES('23:40 (BDT)', '23:45 (BDT)', '23:55 (BDT)', '23:56 (BDT)', '23:57 (BDT)', '23:57 (BDT)', '23:58 (BDT)', '24 Feb 2017', 'Feb');";
//            }
//            else if(i==24){
//                query = "INSERT INTO prayer_times (fajr, sunrise, dhuar, asr, sunset, maghrib, isha, date, month) VALUES('00:01 (BDT)', '00:02 (BDT)', '00:02 (BDT)', '00:03 (BDT)', '00:04 (BDT)', '00:04 (BDT)', '00:05 (BDT)', '25 Feb 2017', 'Feb');";
//            }
//            else {

                query = "INSERT INTO prayer_times (fajr, sunrise, dhuar, asr, sunset, maghrib, isha, date, month) VALUES('" + fajr + "', '" + sunrise + "', '" + dhuar + "', '" + asr + "', '" + sunset + "', '" + maghrib + "', '" + isha + "', '" + date + "', '" + month + "');";
//            }
            db.execSQL(query);
        }
        db.close();

        Log.d("Insertion", "SuccessfullyInserted");

    }




}
