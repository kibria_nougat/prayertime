package com.powergroupbd.prayertime;

import com.powergroupbd.prayertime.model.Route;

import java.util.List;

/**
 * Created by Office on 2/14/2017.
 */

public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
