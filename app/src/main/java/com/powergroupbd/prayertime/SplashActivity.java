package com.powergroupbd.prayertime;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.location.LocationRequest;
import com.powergroupbd.prayertime.database.DatabaseHandler;
import com.powergroupbd.prayertime.model.Timings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.functions.Action1;

public class SplashActivity extends AppCompatActivity {

    rx.Subscription subscription;
    String lat, lng;
    SharedPreferences sharedPref;
    static int HAS_DATABASE_BEEN_UPGRADED;
    ArrayList<Timings> prayerTimesArrayList;
    MaterialDialog materialDialog;
    boolean flag = false;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AppEventsLogger.activateApp(this);
        sharedPref = this.getSharedPreferences("MyPrayerTimes_SharedPreference", Context.MODE_PRIVATE);


        TextView header = (TextView)findViewById(R.id.headername);
        TextView subheader = (TextView)findViewById(R.id.subname);

        header.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/ArabDances.ttf"));


        subheader.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/ArabDances.ttf"));
    }


    @Override
    protected void onStop() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Waiter().execute();


    }




    //*** Methods for google fused location api

    private void getMyLocation() {
        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(1)
                .setInterval(100);

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(SplashActivity.this);
        subscription = locationProvider.getUpdatedLocation(request)
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {

                        doSthImportantWithObtainedLocation(location);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }

    public String getCurrentTimeZone() {
        java.util.TimeZone timeZone = java.util.TimeZone.getDefault();
        String currentTimeZoneID = timeZone.getID();
        String displayName = timeZone.getDisplayName(true, TimeZone.SHORT, Locale.getDefault());
        Log.d("TimeZooneOffset", "Display Name1 " + displayName);

        return currentTimeZoneID.toString();
    }

    private void doSthImportantWithObtainedLocation(Location location) {

        lat = String.valueOf(location.getLatitude());
        lng = String.valueOf(location.getLongitude());
        getCityName(location.getLatitude(), location.getLongitude());
        //                  Toast.makeText(getActivity(), "lat: " + lat + "\n lng: " + lng, Toast.LENGTH_SHORT).show();
        GetPrayerTimes getPrayerTimes = new GetPrayerTimes(SplashActivity.this);
        getPrayerTimes.getPrayerTimeForSpecificMonthAndYear(lat, lng, getCurrentTimeZone());
    }


    //***data class;

    public class GetPrayerTimes {

        Context mContext;


        public GetPrayerTimes(Context context) {
            mContext = context;
        }

        public void getPrayerTimeForSpecificMonthAndYear(final String lat, final String lng, final String timeZone) {

            String[] separated = timeZone.split("/");
            String continent = separated[0];
            String city = separated[1];
            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;

            prayerTimesArrayList = new ArrayList<Timings>();

            String url = "http://api.aladhan.com/calendar?latitude=" + lat + "&longitude=" + lng + "&timezonestring=" + continent + "%2F" + city + "&method=1&month=" + month + "&year=" + year;
            Log.d("URL first", url);
            showpDialog();
            JsonObjectRequest getSingleDayPrayerTimesRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responseObject) {

                    try {
                        Log.d("ResponseStatus", "Responded");
                        JSONArray data = responseObject.getJSONArray("data");

                        for (int i = 0; i < data.length(); i++) {

                            JSONObject jsonObject = data.getJSONObject(i);

                            JSONObject timings = jsonObject.getJSONObject("timings");
                            String fajr = timings.getString("Fajr");

                            if (i == 0) {
                                String[] separated = fajr.split(" ");
                                String temp1 = separated[1].replace("(", "");
                                String myTimeZone = temp1.replace(")", "");
                                SharedPreferences.Editor editor = sharedPref.edit();
                                Log.d("MyTimeZoneFor", "This is my timeZone  " + myTimeZone);
                                editor.putString("MyTimeZone", myTimeZone);
                            }
                            String sunrise = timings.getString("Sunrise");
                            String dhuhr = timings.getString("Dhuhr");
                            String asr = timings.getString("Asr");
                            String sunset = timings.getString("Sunset");
                            String maghrib = timings.getString("Maghrib");
                            String isha = timings.getString("Isha");

                            JSONObject date = jsonObject.getJSONObject("date");
                            String readableDate = date.getString("readable");

                            String[] parts = readableDate.split(" ");
                            String month = parts[1];

                            Timings prayerTimes = new Timings(fajr, sunrise, dhuhr, asr, sunset, maghrib, isha, readableDate, month);
                            prayerTimesArrayList.add(prayerTimes);

                            Log.d("DataParsed", "" + readableDate);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    getNextMonthPrayerTimes(lat, lng, timeZone);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("NetWorkError", error.toString());
                    Toast.makeText(mContext, "Network error!", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(getSingleDayPrayerTimesRequest);
        }

        private void getNextMonthPrayerTimes(String lat, String lng, String timeZone) {

            String[] separated = timeZone.split("/");
            String continent = separated[0];
            String city = separated[1];
            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;
            int nextMonth = 0;
            if (month == 12) {
                nextMonth = 1;
                year = year + 1;
            } else {
                nextMonth = month + 1;
            }

            String url = "http://api.aladhan.com/calendar?latitude=" + lat + "&longitude=" + lng + "&timezonestring=" + continent + "%2F" + city + "&method=4&month=" + nextMonth + "&year=" + year;
            Log.d("URL", url);
            showpDialog();
            JsonObjectRequest getSingleDayPrayerTimesRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responseObject) {

                    try {
                        Log.d("ResponseStatus", "Responded");
                        JSONArray data = responseObject.getJSONArray("data");

                        for (int i = 0; i < data.length(); i++) {

                            JSONObject jsonObject = data.getJSONObject(i);

                            JSONObject timings = jsonObject.getJSONObject("timings");
                            String fajr = timings.getString("Fajr");

                            if (i == 0) {
                                String[] separated = fajr.split(" ");
                                String temp1 = separated[1].replace("(", "");
                                String myTimeZone = temp1.replace(")", "");
                                SharedPreferences.Editor editor = sharedPref.edit();
                                Log.d("MyTimeZoneFor", "This is my timeZone  " + myTimeZone);
                                editor.putString("MyTimeZone", myTimeZone);
                            }
                            String sunrise = timings.getString("Sunrise");
                            String dhuhr = timings.getString("Dhuhr");
                            String asr = timings.getString("Asr");
                            String sunset = timings.getString("Sunset");
                            String maghrib = timings.getString("Maghrib");
                            String isha = timings.getString("Isha");

                            JSONObject date = jsonObject.getJSONObject("date");
                            String readableDate = date.getString("readable");

                            String[] parts = readableDate.split(" ");
                            String month = parts[1];

                            Timings prayerTimes = new Timings(fajr, sunrise, dhuhr, asr, sunset, maghrib, isha, readableDate, month);
                            prayerTimesArrayList.add(prayerTimes);

                            Log.d("DataParsed", "" + readableDate);
                        }

                        HAS_DATABASE_BEEN_UPGRADED = sharedPref.getInt("HAS_DATABASE_BEEN_UPGRADED", 0);
                        if (HAS_DATABASE_BEEN_UPGRADED == 0) {

                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putInt("HAS_DATABASE_BEEN_UPGRADED", 1);
                            editor.commit();
                            DatabaseHandler databaseHandler = new DatabaseHandler(SplashActivity.this);

                            Log.d("ListSize", "Prayer Time List Size " + prayerTimesArrayList.size());
                            databaseHandler.insertPrayerTimesIntoDatabase(prayerTimesArrayList);

                            Log.d("IfTableDataDeleted", "No of Table 1  " + databaseHandler.getNumberOFDaysInMonth("Feb"));
                            Log.d("IfTableDataDeleted", "No of Table 2  " + databaseHandler.getNumberOFDaysInMonth("Mar"));
                        } else if (HAS_DATABASE_BEEN_UPGRADED == 2) {

                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putInt("HAS_DATABASE_BEEN_UPGRADED", 1);
                            editor.commit();

                            DatabaseHandler databaseHandler = new DatabaseHandler(SplashActivity.this);
                            databaseHandler.deleteAllDataFromTable();
                            Log.d("IfTableDataDeleted2", "Prayer Time List Size " + prayerTimesArrayList.size());
                            databaseHandler.insertPrayerTimesIntoDatabase(prayerTimesArrayList);
                            Log.d("IfTableDataDeleted2", "No of Table 1  " + databaseHandler.getNumberOFDaysInMonth("Feb"));
                            Log.d("IfTableDataDeleted2", "No of Table 2  " + databaseHandler.getNumberOFDaysInMonth("Mar"));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    getHijriDateOfToday();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("NetWorkError", error.toString());
                    Toast.makeText(mContext, "Network error!", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(getSingleDayPrayerTimesRequest);
        }

        private void getHijriDateOfToday() {

            String url = "http://api.aladhan.com/gToH?";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responseObject) {

                    Log.d("MyHijriDate", "Responded");
                    try {

                        JSONObject date = responseObject.getJSONObject("data");
                        JSONObject hijri = date.getJSONObject("hijri");
                        String day = hijri.getString("day");
                        JSONObject month = hijri.getJSONObject("month");

                        Log.d("MyHijriDate", "Day " + day);
                        String year = hijri.getString("year");
                        Log.d("MyHijriDate", "Year " + year);

                        String monthNameInUniCode = month.getString("en");
                        String monthName = decodeUnicode(monthNameInUniCode);

                        Log.d("MyHijriDate", "Month " + monthName);

                        Log.d("MyHijriDate", day + " " + monthNameInUniCode + " " + year);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("HijriDate", day + " " + monthName + " " + year);
                        editor.commit();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

//                    enableAdd();
//                    new TestNet().execute();

                    if (!isFinishing()) {
                        goToHomeFragment();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("NetWorkError", error.toString());
                    Toast.makeText(SplashActivity.this, "Network error!", Toast.LENGTH_SHORT).show();
                }
            });

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(request);
        }

        private void showpDialog() {
            if (!isFinishing()) {
                materialDialog = new MaterialDialog.Builder(SplashActivity.this)
                        .title(R.string.findinglocation)
                        .content(R.string.pleasewait)
                        .progress(true, 0)
                        .titleColor(Color.parseColor("#263238"))
                        .backgroundColor(Color.parseColor("#ffffff"))
                        .contentColor(Color.parseColor("#37474f"))
                        .cancelable(true)
                        .widgetColor(Color.parseColor("#1d8049"))
                        .show();
            }

        }

        private void hidepDialog() {
            if (materialDialog != null) {
                materialDialog.dismiss();
            }
        }

        private void goToHomeFragment() {

            hidepDialog();

            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }




    }


    private static String decodeUnicode(String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len; ) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';
                    else if (aChar == 'n')
                        aChar = '\n';
                    else if (aChar == 'f')
                        aChar = '\f';
                    outBuffer.append(aChar);
                }
            } else
                outBuffer.append(aChar);
        }
        return outBuffer.toString();
    }

    private void getCityName(double MyLat, double MyLong) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<android.location.Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String cityName = addresses.get(0).getAddressLine(0);
        String stateName = addresses.get(0).getAddressLine(1);
        String countryName = addresses.get(0).getAddressLine(2);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("MyCurrentCityName", stateName);
        editor.commit();

        Log.d("MyCityName", cityName);
        Log.d("MyCityName", stateName);
        Log.d("MyCityName", countryName);

    }


    private class Waiter extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {


            try {
                Thread.sleep(2000);
            } catch (Exception e) {

                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void aVoid) {

            if (isInternetAvailable() && isGPSEnable()) {
                Log.d("AppStatus", "APP is running first");
                getMyLocation();
            } else {
                int is_app_running_first = sharedPref.getInt("IS_APP_RUNNING_FIRST", 0);

                if (is_app_running_first == 1) {
                    Log.d("AppStatus", "APP is not running first");
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else {
                    if (!isFinishing()) {
                        final AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this);
                        dialog.setMessage(R.string.internetmsg);
                        dialog.setPositiveButton(R.string.opensettings, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                            }
                        });
                        dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        dialog.show();
                    }
                }
            }


        }

    }


    private boolean isInternetAvailable() {
        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true)
            return true;
        else
            return false;
    }

    private boolean isGPSEnable() {

        LocationManager lm = null;
        boolean gps_enabled = false, network_enabled = false;

        if (lm == null)
            lm = (LocationManager) SplashActivity.this.getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            if (!isFinishing()) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this);
                dialog.setMessage(R.string.internetmsg);
                dialog.setPositiveButton(R.string.opensettings, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        SplashActivity.this.startActivity(myIntent);
                    }
                });
                dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        flag = false;
                    }
                });
                dialog.show();
            }
        } else {
            return true;
        }

        return flag;
    }

}
