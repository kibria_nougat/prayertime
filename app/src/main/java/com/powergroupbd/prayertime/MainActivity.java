package com.powergroupbd.prayertime;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.powergroupbd.prayertime.fragments.FragmentHome;
import com.powergroupbd.prayertime.fragments.FragmentMosques;
import com.powergroupbd.prayertime.fragments.FragmentOthers;
import com.powergroupbd.prayertime.fragments.FragmentPrayerTimings;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;


public class MainActivity extends AppCompatActivity {

    private AdView mAdView;
    BottomBar bottomBar;
    android.support.v4.app.Fragment fragment = null;
    boolean shouldShowAdd = false;
    InterstitialAd fullScreenAdd;
    AdRequest fullScreenAdRequest;
    boolean isActivityRunning = true;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // viewInitialization();
        pb = (ProgressBar) findViewById(R.id.pb);
        Toast.makeText(getApplicationContext(), R.string.pleasewait, Toast.LENGTH_LONG).show();
        enableAdd();
        new TestNet().execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityRunning = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityRunning = false;
    }

    private void viewInitialization() {
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        mAdView.loadAd(adRequest);


        pb.setVisibility(View.INVISIBLE);
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setSelected(true);
        bottomBar.setDefaultTab(R.id.tab_home);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_home) {

                    fragment = FragmentHome.newInstance();

                } else if (tabId == R.id.tab_prayer_timings) {

                    fragment = FragmentPrayerTimings.newInstance();

                } else if (tabId == R.id.tab_mosques) {

                    fragment = FragmentMosques.newInstance();

                } else if (tabId == R.id.tab_others) {

                    fragment = FragmentOthers.newInstance();

                }

                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.main_activity_frameLayout, fragment);
                    fragmentTransaction.commit();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {


        buildAlertExit(this);

    }

    private void buildAlertExit(final Context c) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage(
                getString(R.string.exiting))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                finish();
                            }
                        })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int id) {
                        dialog.cancel();

                    }
                });
        final AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
    }

    private void enableAdd() {

        fullScreenAdd = new InterstitialAd(MainActivity.this);
        fullScreenAdd.setAdUnitId(getResources().getString(R.string.test_admob_full_screen_ad_id));
        fullScreenAdRequest = new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build();
        fullScreenAdd.loadAd(fullScreenAdRequest);

        fullScreenAdd.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {

                Log.i("FullScreenAdd", "Loaded successfully");

                shouldShowAdd = true;

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.i("FullScreenAdd", "failed to Load");

                shouldShowAdd = false;
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if (isActivityRunning) {
                    viewInitialization();
                }
            }

        });
    }

    private void loadHome() {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity_frameLayout, FragmentHome.newInstance());
        fragmentTransaction.commit();
    }


    private class TestNet extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {


            if (shouldShowAdd) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isActivityRunning) {
                            fullScreenAdd.show();
                            shouldShowAdd = false;
                        }
                    }
                });

            } else {
                if (isActivityRunning) {
                    viewInitialization();
                }
            }


        }
    }
}
