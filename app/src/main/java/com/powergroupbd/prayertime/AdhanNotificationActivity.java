package com.powergroupbd.prayertime;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.powergroupbd.prayertime.service.AdhanPlayerService;

public class AdhanNotificationActivity extends AppCompatActivity {


    Intent adhanServiceIntent;
    AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adhan_notification);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        mAdView.loadAd(adRequest);
        Button buttonStopAlarm = (Button) findViewById(R.id.buttonStopAlarm);
        TextView prayer_Name_TextView = (TextView) findViewById(R.id.prayer_Name_TextView);
        adhanServiceIntent = new Intent(AdhanNotificationActivity.this, AdhanPlayerService.class);
        Intent intent = getIntent();
        String prayerName = intent.getStringExtra("prayer_name");

        if (!isMyServiceRunning(this)) {

            adhanServiceIntent.putExtra("should_fajr_adhan_play", intent.getIntExtra("should_fajr_adhan_play", 4));
            adhanServiceIntent.putExtra("should_play_adhan_or_default_alarm", intent.getIntExtra("should_play_adhan_or_default_alarm", 6));
            adhanServiceIntent.putExtra("Is_play_nothing", intent.getIntExtra("Is_play_nothing", 0));
            startService(adhanServiceIntent);
        }
        prayer_Name_TextView.setText(prayerName);


        buttonStopAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopService(adhanServiceIntent);
                finish();

            }
        });
    }

    public static boolean isMyServiceRunning(Context c) {
        ActivityManager manager = (ActivityManager) c.getSystemService(c.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {

            if (Constants.SERVICE_PACKAGE_NAME
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
