package com.powergroupbd.prayertime;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Office on 2/9/2017.
 */

public class AlarmSetter {

    public void setAlarm(Context context, String fajrTime, String dhuhrTime, String asrTime, String maghribTime, String ishaTime){

        SimpleDateFormat msdf = new SimpleDateFormat("HH:mm");
        SharedPreferences sharedPref = context.getSharedPreferences("MyPrayerTimes_SharedPreference", Context.MODE_PRIVATE);
        String timeZone = sharedPref.getString("MyTimeZone", "");
        msdf.setTimeZone(TimeZone.getTimeZone(timeZone));

        try {
            Date fajr = msdf.parse(fajrTime);
            Date dhuhr = msdf.parse(dhuhrTime);
            Date asr = msdf.parse(asrTime);
            Date maghrib = msdf.parse(maghribTime);
            Date isha = msdf.parse(ishaTime);
            Date alarmTimeBeforeTwelve = msdf.parse("23:59 "+timeZone);

            long fajrAlarmTime = fajr.getTime();
            long dhuhrAlarmTime = dhuhr.getTime();
            long asrAlarmTime = asr.getTime();
            long maghribAlarmTime = maghrib.getTime();
            long ishaAlarmTime = isha.getTime();
            long alarmTimeBeforeTwelveOclock = alarmTimeBeforeTwelve.getTime();

            triggerAlarmForFajr(context.getApplicationContext(), fajrAlarmTime);
            triggerAlarmForDhuhr(context.getApplicationContext(), dhuhrAlarmTime);
            triggerAlarmForAsr(context.getApplicationContext(), asrAlarmTime);
            triggerAlarmForMaghrib(context.getApplicationContext(), maghribAlarmTime);
            triggerAlarmForIsha(context.getApplicationContext(), ishaAlarmTime);
            triggerAlarmAfterTwelveOClock(context.getApplicationContext(), alarmTimeBeforeTwelveOclock);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    public void triggerAlarmForFajr(Context context, long alramTime){

        long currentTime = getCurrentTimeInMillis();

        if(currentTime<alramTime){

            long alarmTriggerTime = alramTime - currentTime;

            PendingIntent myPendingIntent1 = null;
            AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(context.ALARM_SERVICE);
            final Intent myIntent = new Intent(context.getApplicationContext(), AlarmReceiverService.class);
            myIntent.putExtra("PrayerName", "Fajr");
            myPendingIntent1 = PendingIntent.getService(context.getApplicationContext(), 1, myIntent, myPendingIntent1.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + alarmTriggerTime, myPendingIntent1);
        }

    }

    public void triggerAlarmForDhuhr(Context context, long alramTime){

        long currentTime = getCurrentTimeInMillis();

        if(currentTime<alramTime) {

            long alarmTriggerTime = alramTime - currentTime;

            PendingIntent myPendingIntent2 = null;
            AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(context.ALARM_SERVICE);
            final Intent myIntent = new Intent(context.getApplicationContext(), AlarmReceiverService.class);
            myIntent.putExtra("PrayerName", "Dhuhr");
            myPendingIntent2 = PendingIntent.getService(context.getApplicationContext(), 2, myIntent, myPendingIntent2.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + alarmTriggerTime, myPendingIntent2);
        }
    }

    public void triggerAlarmForAsr(Context context, long alramTime){

        long currentTime = getCurrentTimeInMillis();

        if(currentTime<alramTime) {

            long alarmTriggerTime = alramTime - currentTime;

            PendingIntent myPendingIntent3 = null;
            AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(context.ALARM_SERVICE);
            final Intent myIntent = new Intent(context.getApplicationContext(), AlarmReceiverService.class);
            myIntent.putExtra("PrayerName", "Asr");
            myPendingIntent3 = PendingIntent.getService(context.getApplicationContext(), 3, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + alarmTriggerTime, myPendingIntent3);
        }
    }

    public void triggerAlarmForMaghrib(Context context, long alramTime){

        long currentTime = getCurrentTimeInMillis();

        if(currentTime<alramTime) {

            long alarmTriggerTime = alramTime - currentTime;

            PendingIntent myPendingIntent4 = null;
            AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(context.ALARM_SERVICE);
            final Intent myIntent = new Intent(context.getApplicationContext(), AlarmReceiverService.class);
            myIntent.putExtra("PrayerName", "Maghrib");
            myPendingIntent4 = PendingIntent.getService(context.getApplicationContext(), 4, myIntent, myPendingIntent4.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + alarmTriggerTime, 0, myPendingIntent4);
        }
    }

    public void triggerAlarmForIsha(Context context, long alramTime){

        long currentTime = getCurrentTimeInMillis();
        Log.d("AlarmTime", alramTime+" Isha Alarm time");

        if(currentTime<alramTime) {

            long alarmTriggerTime = alramTime - currentTime;

            PendingIntent myPendingIntent5 = null;
            AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(context.ALARM_SERVICE);
            final Intent myIntent = new Intent(context.getApplicationContext(), AlarmReceiverService.class);
            myIntent.putExtra("PrayerName", "Isha");
            myPendingIntent5 = PendingIntent.getService(context.getApplicationContext(), 5, myIntent, myPendingIntent5.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + alarmTriggerTime, myPendingIntent5);
        }
    }


    public void triggerAlarmAfterTwelveOClock(Context context, long alramTime) {

        Log.d("AlarmTime", alramTime+" Twelve O clock");

        long currentTime = getCurrentTimeInMillis();

        if(currentTime<alramTime) {

            long alarmTriggerTime = alramTime - currentTime;

            PendingIntent myPendingIntent6 = null;
            AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(context.ALARM_SERVICE);
            final Intent myIntent = new Intent(context.getApplicationContext(), AlarmReceiverService.class);
            myIntent.putExtra("PrayerName", "NewDay");
            myPendingIntent6 = PendingIntent.getService(context.getApplicationContext(), 6, myIntent, myPendingIntent6.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + alarmTriggerTime + 60000, myPendingIntent6); //60000=1s
        }

    }


    public long getCurrentTimeInMillis(){
        Calendar c = Calendar.getInstance();
        long now = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long currentTime = now - c.getTimeInMillis();
        return currentTime;
    }

    public String getCurrentTimeZone() {
        java.util.TimeZone timeZone = java.util.TimeZone.getDefault();

        String displayName = timeZone.getDisplayName(true, TimeZone.SHORT, Locale.getDefault());
        Log.d("TimeZooneOffset", "Display Name1 "+ displayName);

        return displayName.toString();
    }


}
